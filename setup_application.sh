#!/bin/bash

if [[ -z "$1" ]]; then
    echo "you need to provide an application id as an argument to this script"
    exit 22
fi

if ttnctl user > /dev/null; then
    echo "User logged in."
else
    rc=$?
    echo "Not logged in, please login using ttnctl"
    exit $rc
fi


filter_ttn(){
    sed '/^.*INFO.*$/d' | sed '/^$/d'
}

v=$(ttnctl --handler-id meshed-handler application select "$1" > /dev/null)
rc=$?
if [[ $rc != 0 ]]; then
    echo "select application error"
    exit $rc    
fi


app_eui=$(ttnctl --handler-id meshed-handler application info | filter_ttn | yq -r '.EUIs[0]')
echo "Selected $1 - $app_eui"

ttnctl  --handler-id meshed-handler application pf set decoder decoder.js