#!/bin/bash

# to use do grep <device name> devices  | xargs ./setup_sensor.sh
# devices should look like this:
# APPEUI DEV_NAME DEV_EUI APP_KEY
# APPEUI DEV_NAME DEV_EUI APP_KEY

APP_EUI=$1
DEV_NAME=$2
DEV_EUI=$3
APP_KEY=$4
stty -F /dev/ttyACM0 115200 -parity cs8 -echo

cat /dev/ttyACM0 &

bgPID=$!
sleep 3
echo "int 600"              > /dev/ttyACM0
echo "appeui ${APP_EUI}"    > /dev/ttyACM0
echo "deveui ${DEV_EUI}"    > /dev/ttyACM0
echo "key ${APP_KEY}"       > /dev/ttyACM0
echo "info"                 > /dev/ttyACM0
sleep 3
echo "reset"                > /dev/ttyACM0
sleep 1

kill $bgPID 2>/dev/null