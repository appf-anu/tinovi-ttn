# Tinovi TTN

### TTN setup

1. create application on [ttn console](https://console.thethingsnetwork.org/applications/add), use the meshed-au router.
2. Add your devices

### Device setup

1. setup the `devices` file with each of your devices on its own line with the values `APP_EUI DEV_ID DEV_EUI APP_KEY`, space separated.
2. plug the device into a linux pc
3. run the setup_sensor.sh script like this:
    ```bash 
    grep DEV_NAME devices | xargs ./setup_sensor.sh
    ```
    
### Telegraf setup

1. Setup your InfluxDB server
2. Create an access key on ttn for your application
2. run telegraf like this:
    ```bash
    export TTN_APP_NAME=<your-app-name>
    export TTN_PASSWORD=<your-access-key
    export TTN_METRIC_NAME=tinovi-lorawan
    export INFLUXDB_URL=<your-influxdb-url>
    export INFLUXDB_DATABASE=<your-influxdb-database>
    export INFLUXDB_USER=<your-influxdb-user>
    export INFLUXDB_PASSWORD=<your-influxdb-password>
    telegraf --config telegraf.toml
    ```