
/**
 * calculates Saturated Vapour Pressure (eₛ) in kPa from Temperature in °C 
 * using the Buck (1996) equation
 * Source:
 *     https://en.wikipedia.org/wiki/Arden_Buck_equation
 *
 * Note: the official equation calls for aw=6.1121 and ai=6.1115 however
 * this resulted in calculations that were exactly 10x higher than expected,
 * so these values have been scaled so aw=0.61121 and ai=0.61115
 * 
 * @param  {Number} Temperature in °C
 * @return {Number} Saturated Vapour Pressure in kPa
 */
function getSaturatedVapourPressure_kPa(temp_C){
    if (temp_C > 0.0) {
        return 0.61121 * Math.exp( (18.678 - (temp_C/234.5)) * (temp_C / (257.14 + temp_C)) );
    }
    return 0.61115 * Math.exp( (23.036 - (temp_C/333.7)) * (temp_C / (279.82 + temp_C)) );    
}


/**
 * calculates Actual Vapour Pressure (eₐ) in kPa from Saturated Vapour Pressure (eₛ) in kPa and 
 * Relative Humidity in percent(%)
 *
 * @param  {Number} Relative Humidity in percent(%)
 * @param  {Number} Saturated Vapour Pressure (eₛ) in kPa
 * @return {Number} Actual Vapour Pressure (eₐ) in kPa
 */
function getActualVapourPressure_kPa(rh_pc, eₛ){
    return eₛ * (rh_pc / 100);
}

/**
 * calculates the Vapour Pressure Deficit in kPa from Saturated Vapour Pressure (eₛ) and
 * Actual Vapor Pressure (eₐ)
 * 
 * Note: always returns a negative number because VPD is a deficit
 * 
 * @param  {Number} Saturated Vapour Pressure (eₛ) in kPa
 * @param  {Number} Actual Vapor Pressure (eₐ) in kPa
 * @return {Number} Vapour Pressure Deficit (VPD) in kPa
 */
function getVapourPressureDeficit_kPa(eₛ, eₐ){
    return (eₛ - eₐ) * -1;
}


/**
 * calculates the Mixing Ratio in g/kg from Atmospheric Pressure in hPa and Actual Vapour 
 * Pressure (eₐ) in kPa
 * 
 * @param  {Number} Atmospheric Pressure in hPa
 * @param  {Number} Actual Vapour Pressure (eₐ) in kPa
 * @return {Number} Mixing Ratio in g/kg
 */
function getMixingRatio_gkg(pres_hPa, eₐ){
    return 621.97 * (eₐ / ((pres_hPa/10) - eₐ))
}


/**
 * calculates the Absolute Humidity in kg/m³ from Temperature in °C and Actual Vapour 
 * Pressure (eₐ) in kPa
 * 
 * @param  {Number} Temperature in °C
 * @param  {Number} Actual Vapour Pressure (eₐ) in kPa
 * @return {Number} Absolute Humidity in kg/m³
 */
function getAbsoluteHumidity_kgm3(temp_C, eₐ){
    return (eₐ / (461.5 * (temp_C + 273.15))) * 1000
}

var bytesToInt = function( /*byte[]*/ byteArray, dev) {
    var value = 0;
    for (var i = 0; i < byteArray.length; i++) {
        value = (value * 256) + byteArray[i];
    }
    return value / dev;
};


var bytesToSignedInt = function(bytes, dev) {
    var sign = bytes[0] & (1 << 7);
    var x = ((bytes[0] & 0xFF) << 8) | (bytes[1] & 0xFF);
    if (sign) {
        x = 0xFFFF0000 | x;
    }
    return x / dev;
};

function removeNaN(data){
    var keys = Object.keys(data);
    for (var i =0; i < keys.length; i++) {
        if (data[keys[i]] !== data[keys[i]]){
            delete data[keys[i]];
        }
    }
    return data;
}

function Decoder(bytes, port) {
    // https://d3s5r33r268y59.cloudfront.net/datasheets/13688/2020-01-03-13-57-50/PM-IO-5-SM.pdf
    var decoded = {};
    var pos = 1;
    decoded.sys_ValveState_bool = Boolean((bytes[0] >> 7) & 1);
    decoded.sys_WaterLeak_bool = Boolean((bytes[0] >> 6) & 1);
    decoded.sys_Battery_mV = (2.8+(0.014*bytes[pos++]))*1000.0;

    if (((bytes[0] >> 0) & 1) === 1) { //1st SOIL sensor
        decoded.soil_DielectricPermittivity_scalar = bytesToInt(bytes.slice(pos, pos + 2), 100);
        pos = pos + 2;
        decoded.soil_ElectricalConductivity_mSm = bytesToInt(bytes.slice(pos, pos + 2), 10);
        pos = pos + 2;
        decoded.soil_Temperature_C = bytesToSignedInt(bytes.slice(pos, pos + 2), 100);
        pos = pos + 2;
        decoded.soil_VolumetricWaterContent_percent = bytesToInt(bytes.slice(pos, pos + 2), 1);
        pos = pos + 2;
    }
    if (((bytes[0] >> 1) & 1) === 1) { //BME280
        decoded.air_Temperature_C = bytesToSignedInt(bytes.slice(pos, pos + 2), 100);
        pos = pos + 2;
        decoded.air_RelativeHumidity_pc = bytesToInt(bytes.slice(pos, pos + 2), 100);
        pos = pos + 2;
        decoded.air_PressureQFE_hPa = (bytesToInt(bytes.slice(pos, pos + 2), 1) + 50000)/100.0;
        pos = pos + 2;
        var eₛ = getSaturatedVapourPressure_kPa(decoded.air_Temperature_C);
        decoded.air_SaturatedVapourPressure_kPa = eₛ;
        var eₐ = getActualVapourPressure_kPa(decoded.air_RelativeHumidity_pc, eₛ);
        decoded.air_ActualVapourPressure_kPa = eₐ;
        var vpd = getVapourPressureDeficit_kPa(eₛ, eₐ);
        decoded.air_VapourPressureDeficit_kPa = vpd;
        var mr_gkg = getMixingRatio_gkg(decoded.air_PressureQFE_hPa, eₛ);
        decoded.air_MixingRatio_gkg = mr_gkg;
        var ah_kgm3 = getAbsoluteHumidity_kgm3(decoded.air_Temperature_C, eₐ);
        decoded.air_AbsoluteHumidity_kgm3 = ah_kgm3;
    }
    if (((bytes[0] >> 2) & 1) === 1) { //OPT3001
        decoded.lux = bytesToInt(bytes.slice(pos, pos + 4), 100);
        pos = pos + 4;
    }
    if (((bytes[0] >> 4) & 1) === 1) { //PULSE
        decoded.pulse = bytesToInt(bytes.slice(pos, pos + 4), 1);
        pos = pos + 4;
    }
    if (((bytes[0] >> 3) & 1) === 1) { //2nd soil sensor
        decoded.soil1_DielectricPermittivity_mSm = bytesToInt(bytes.slice(pos, pos + 2), 100);
        pos = pos + 2;
        decoded.soil1_ElectricalConductivity_mSm = bytesToInt(bytes.slice(pos, pos + 2), 10);
        pos = pos + 2;
        decoded.soil1_Temperature_C = bytesToSignedInt(bytes.slice(pos, pos + 2), 100);
        pos = pos + 2;
        decoded.soil1_VolumetricWaterContent_pc = bytesToInt(bytes.slice(pos, pos + 2), 1);
        pos = pos + 2;
    }
    decoded.somenan = Number(undefined);
    decode = removeNaN(decoded);
    console.log(decoded);
    return decoded;
}