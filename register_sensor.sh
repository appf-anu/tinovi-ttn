#!/bin/bash

if [[ -z "$1" ]]; then
    echo "you need to provide a device id as an argument to this script"
    exit 22
fi

if ttnctl user > /dev/null; then
    echo "User logged in."
else
    rc=$?
    echo "Not logged in, please login using ttnctl"
    exit $rc
fi


filter_ttn(){
    sed '/^.*INFO.*$/d' | sed '/^$/d'
}

get_device_list(){
    ttnctl --handler-id meshed-handler device list | filter_ttn | tail -n +2 | sed 's/\t\+/ /g'
}

get_device_info(){
    local DEV_ID=$1
    ttnctl --handler-id meshed-handler device info "$DEV_ID" | filter_ttn | sed -e 's/^[ \t]*//' 
}

while IFS=' ' read -r DEV_ID APP_EUI DEV_EUI DEV_ADDR DESC; do
    if grep --quiet "$DEV_EUI" devices; then
        continue
    fi

    APP_KEY=$(get_device_info $DEV_ID | yq -r '.AppKey')
    echo "$APP_EUI $DEV_ID $DEV_EUI $APP_KEY" >> devices
done <<< $(get_device_list)

while IFS=' ' read -r DEV_ID APP_EUI DEV_EUI DEV_ADDR DESC; do
    if [[ "$1" == "$DEV_ID" ]];then
        echo "$1 already exists, choose another device id"
        exit 22
    fi
done <<< $(get_device_list)

ttnctl --handler-id meshed-handler device register $1


while IFS=' ' read -r DEV_ID APP_EUI DEV_EUI DEV_ADDR DESC; do
    if grep --quiet "$DEV_EUI" devices; then
        continue
    fi
    APP_KEY=$(get_device_info $DEV_ID | yq -r '.AppKey')
    echo "$APP_EUI $DEV_ID $DEV_EUI $APP_KEY" >> devices
done <<< $(get_device_list)
